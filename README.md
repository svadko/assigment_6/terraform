# Terraform Infrastructure as Code Documentation

## Overview

This Terraform script defines the infrastructure for creating GitLab projects and a DigitalOcean Droplet. The Droplet is provisioned with an SSH key and Ansible for further configuration.

## GitLab Projects

### Project 1 - `sample_project_1`

```hcl
resource "gitlab_project" "sample_project_1" {
    name = "project_1"
}
```

### Project 2 - `sample_project_2`

```hcl
resource "gitlab_project" "sample_project_2" {
    name = "project_2"
}
```

### Project 3 - `sample_project_3`

```hcl
resource "gitlab_project" "sample_project_3" {
    name = "project_3"
}
```

## DigitalOcean Resources

### SSH Key - `my_ssh_key`

```hcl
resource "digitalocean_ssh_key" "my_ssh_key" {
    name       = "my-ssh-key"
    public_key = file("~/.ssh/id_rsa.pub")
}
```

### Droplet - `my_droplet`

```hcl
resource "digitalocean_droplet" "my_droplet" {
    name   = "my-droplet"
    region = "nyc1"
    size   = "s-1vcpu-1gb"
    image  = "ubuntu-20-04-x64"

    ssh_keys = [digitalocean_ssh_key.my_ssh_key.id]

    provisioner "remote-exec" {
        inline = [
            # Ansible setup commands
        ]

        connection {
            type        = "ssh"
            user        = "root"
            private_key = file("~/.ssh/id_rsa")
            host        = self.ipv4_address
        }
    }
}
```

### Local Variables

```hcl
locals {
    droplet_ip = digitalocean_droplet.my_droplet.ipv4_address
}
```

### Configure SSH for Ansible

```hcl
resource "null_resource" "configure_ssh" {
    depends_on = [digitalocean_droplet.my_droplet]

    provisioner "local-exec" {
        command = "echo 'AllowUsers ansible@${local.droplet_ip}' >> ~/.ssh/authorized_keys"
    }
}
```

### Check Droplet Health

```hcl
resource "null_resource" "check_health" {
    depends_on = [digitalocean_droplet.my_droplet]

    provisioner "local-exec" {
        command = "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ansible@${local.droplet_ip}"
    }
}
```

## Usage

1. Initialize Terraform:

   ```bash
   terraform init
   ```

2. Apply the configuration:

   ```bash
   terraform apply
   ```

3. Destroy the infrastructure when done:
   ```bash
   terraform destroy
   ```

## Notes

- This script assumes you have valid DigitalOcean credentials and GitLab API access.
- Ensure you replace placeholder values and customize the Ansible setup commands as needed.
- Security best practices, such as securely managing private keys, should be followed in a production environment.
