module "project_1" {
  source        = "./modules/gitlab_project"
  gitlab_gitlab_token = var.gitlab_token
  gitlab_project_name = var.gitlab_project_name_1
}

module "project_2" {
  source        = "./modules/gitlab_project"
  gitlab_gitlab_token = var.gitlab_token
  gitlab_project_name = var.gitlab_project_name_2
}

module "project_3" {
  source        = "./modules/gitlab_project"
  gitlab_gitlab_token = var.gitlab_token
  gitlab_project_name = var.gitlab_project_name_3
}

resource "digitalocean_ssh_key" "my_ssh_key" {
  name       = "my-ssh-key"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "digitalocean_droplet" "my_droplet" {
  name   = "my-droplet"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  image  = "ubuntu-20-04-x64"

  ssh_keys = [digitalocean_ssh_key.my_ssh_key.id]
    connection {
      type        = "ssh"
      user        = "root"
      private_key = file("~/.ssh/id_rsa")
      host        = self.ipv4_address
    }
}

module "commands" {
  source        = "./modules/commands"
  command_ip_address = digitalocean_droplet.my_droplet.ipv4_address
}

module "healthcheker" {
  source        = "./modules/healthcheker"
  healthcheker_ip_address = digitalocean_droplet.my_droplet.ipv4_address
}