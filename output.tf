output "public_ip" {
  value = digitalocean_droplet.my_droplet.ipv4_address
}
