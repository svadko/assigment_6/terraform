terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    gitlab = {
      source="gitlabhq/gitlab"
      version="16.0.3"
    }

  }
}