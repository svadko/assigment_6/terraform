variable "command_ip_address" {
  type = string
}

resource "null_resource" "provisioner" {

  connection {
    type        = "ssh"
    user        = "root"
    private_key = file("~/.ssh/id_rsa")
    host        = var.command_ip_address
  }

  provisioner "remote-exec" {
    inline = [
      "sudo adduser --disabled-password --gecos '' ansible",
      "sudo mkdir -p /home/ansible/.ssh",
      "sudo touch /home/ansible/.ssh/authorized_keys",
      "sudo echo '${file("~/.ssh/id_rsa.pub")}' > authorized_keys",
      "sudo mv authorized_keys /home/ansible/.ssh",
      "sudo chown -R ansible:ansible /home/ansible/.ssh",
      "sudo chmod 700 /home/ansible/.ssh",
      "sudo chmod 600 /home/ansible/.ssh/authorized_keys",
      "groupadd ansible",
      "echo \"%ansible    ALL=(ALL:ALL) NOPASSWD:ALL\"  sudo tee -a /etc/sudoers  /dev/null",
      "sudo usermod -aG ansible ansible",
      "sudo apt-get update",
      "sudo apt-get install -y curl openssh-server ca-certificates",
      "sudo apt-get install -y postfix",
      "curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash",
    ]
  }
}