variable "healthcheker_ip_address" {
  type = string
}
resource "null_resource" "check_health" {
    connection {
    type        = "ssh"
    user        = "root"
    private_key = file("~/.ssh/id_rsa")
    host        = var.healthcheker_ip_address
  }

  
  provisioner "local-exec" {
   command = "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ansible@${var.healthcheker_ip_address}"
  }
}